package main

import (
	"log"
	"net/http"

	model "bitbucket.org/w-bt/sharing/app_go/app/models"
	"bitbucket.org/w-bt/sharing/app_go/config"
	conf "bitbucket.org/w-bt/sharing/app_go/config/environments"
	"bitbucket.org/w-bt/sharing/app_go/config/kafka"
	"bitbucket.org/w-bt/sharing/app_go/db"
	"github.com/julienschmidt/httprouter"
)

func init() {
	conf.InitConfig()
	db.Init()
	model.Init()
	kafka.Init()
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	router := httprouter.New()
	config.InitRouter(router)

	log.Fatal(http.ListenAndServe(":9000", router))
}
