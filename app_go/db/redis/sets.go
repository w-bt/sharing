package redis

import (
	"fmt"

	redigo "github.com/garyburd/redigo/redis"
)

// Returns if member is a member of the set stored at key.
// 1 if the element is a member of the set.
// 0 if the element is not a member of the set, or if key does not exist.
func SIsMember(key, value string) (int, error) {
	conn := getConnection()
	if conn != nil {

		defer conn.Close()
		return redigo.Int(conn.Do("SISMEMBER", key, value))
	}
	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)

}

// SADD Add the specified members to the set stored at key
func SAdd(key string, fields ...string) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("SADD", redigo.Args{}.Add(key).AddFlat(fields)...))
	}

	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)

}

// SRem remove the specified members to the set stored at key
func SRem(key string, fields ...string) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("SREM", redigo.Args{}.Add(key).AddFlat(fields)...))
	}

	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)

}

func SMembers(key string) ([]string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		val, err := redigo.Strings(conn.Do("SMEMBERS", key))
		return val, err
	}
	return []string{}, fmt.Errorf("Failed to obtain connection key: %v", key)
}

// SDiff checks value differences between two sets of key
func SDiff(dbname string, keys []string) ([]string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Strings(conn.Do("SDIFF", redigo.Args{}.AddFlat(keys)...))
	}

	return []string{}, fmt.Errorf("Failed to obtain connection key: %v", keys)
}
