// this file will contains most common redis commands
package redis

import (
	"fmt"

	redigo "github.com/garyburd/redigo/redis"
)

// ZAdd add all the specified members with the specified scored to the sorted set stored at key
// updateExistingElementOnly (XX) => only update elements that already exist, never add elements
// alwaysAddNewElement (NX) => don't update already existing elements. Always add new elements
// countUpdatedScore (CH) => return count elements changed (new and updated elements) or just new element
func ZAdd(key string, value interface{}) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("ZADD", redigo.Args{}.Add(key).AddFlat(value)...))
	}
	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)
}

// ZCard return number of elements of sorted set stored at key
func ZCard(key string) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("ZCARD", key))
	}
	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)
}

// ZRange return the specified range of sorted set elements stored at key from lowest to highest
// start and stop is zero (0) based indexes and inclusive
// -1 on stop mean last element of the sorted set
// -2 on stop mean penultimate element of the sorted set
func ZRange(key string, start, stop int) ([]string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		result, err := redigo.Strings(conn.Do("ZRANGE", key, start, stop))
		if err != nil && err == redigo.ErrNil {
			err = nil
		}
		return result, err
	}
	return []string{}, fmt.Errorf("Failed to obtain connection key %s", key)
}

// ZRangeWithScores return the specified range of sorted set elements stored at key from lowest to highest
// start and stop is zero (0) based indexes and inclusive
// -1 on stop mean last element of the sorted set
// -2 on stop mean penultimate element of the sorted set
func ZRangeWithScores(key string, start, stop int) ([]string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		result, err := redigo.Strings(conn.Do("ZRANGE", key, start, stop, "WITHSCORES"))
		if err != nil && err == redigo.ErrNil {
			err = nil
		}
		return result, err
	}
	return []string{}, fmt.Errorf("Failed to obtain connection key %s", key)
}

// ZRem remove specified member from sorted set at stored key
// error is returned when key exists and does not hold a sorted set. non existing member are ignored
func ZRem(key, value string) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("ZREM", key, value))
	}
	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)
}

// ZScore returns the score of member in the sorted set at key.
// If member does not exist in the sorted set, or key does not exist, nil is returned.
func ZScore(key, value string) (string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.String(conn.Do("ZScore", key, value))
	}
	return "", fmt.Errorf("Failed to obtain connection key %s", key)
}

// ZRevRange returns the specified range of elements in the sorted set stored at key
// The elements are considered to be ordered from the highest to the lowest score
// Apart from the reversed ordering, ZREVRANGE is similar to ZRANGE.
func ZRevRange(key string, start, stop int) ([]string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Strings(conn.Do("ZREVRANGE", key, start, stop))
	}
	return []string{}, fmt.Errorf("Failed to obtain connection key %s", key)
}

//ZRangeByScore Returns all the elements in the sorted set at key with a score between min and max
//(including elements with score equal to min or max).
//The elements are considered to be ordered from low to high scores.
func ZRangeByScore(key string, min, max int) ([]string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Strings(conn.Do("ZRANGEBYSCORE", key, min, max))
	}
	return []string{}, fmt.Errorf("Failed to obtain connection key %s", key)
}

//ZRemRangeByScore Removes all elements in the sorted set stored at key with a score between min and max (inclusive).
//Returns number of elements deleted

func ZRemRangeByScore(key string, min, max int) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("ZREMRANGEBYSCORE", key, min, max))
	}
	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)
}
