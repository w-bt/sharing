package redis

import (
	"fmt"

	redigo "github.com/garyburd/redigo/redis"
)

// this file will contains most common redis commands of group "Strings"

// Get gets string value stored in given key
func Get(key string) (string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		result, err := redigo.String(conn.Do("GET", key))
		if err != nil && err == redigo.ErrNil {
			err = nil
		}
		return result, err
	}
	return "", fmt.Errorf("Failed to obtain connection key %s", key)
}

// GetRange gets substring of string stored in given key
func GetRange(key string, start, end int) (string, error) {
	conn := getConnection()
	if conn != nil {

		defer conn.Close()
		return redigo.String(conn.Do("GETRANGE", key, start, end))
	}
	return "", fmt.Errorf("Failed to obtain connection key %s", key)

}

// GetSet automatically set key to value and return the old value stored at key
func GetSet(key, value string) (string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.String(conn.Do("GETSET", key, value))
	}
	return "", fmt.Errorf("Failed to obtain connection key %s", key)

}

// Set set key to hold the string value
func Set(key, value string) (string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.String(conn.Do("SET", key, value))
	}
	return "", fmt.Errorf("Failed to obtain connection key %s", key)

}

// SetEx set key to hold the string value and set key to timeout after a given number of second(s)
func SetEx(key, value string, expseconds int) (string, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.String(conn.Do("SETEX", key, expseconds, value))
	}
	return "", fmt.Errorf("Failed to obtain connection key %s", key)

}

// SetNX set key to hold string value if key does not exist. Do nothing if key exist
// return 1 if the key was set
// return 0 if the key was not set
func SetNX(key, value string) (int, error) {
	conn := getConnection()
	if conn != nil {
		defer conn.Close()
		return redigo.Int(conn.Do("SETNX", key, value))
	}
	return int(0), fmt.Errorf("Failed to obtain connection key %s", key)

}
