package redis

import (
	"log"

	conf "bitbucket.org/w-bt/sharing/app_go/config/environments"
	redigo "github.com/garyburd/redigo/redis"
)

var (
	RedigoHost string
)

const (
	NETWORKTCP = "tcp"
)

func getConnection() redigo.Conn {
	if RedigoHost == "" {
		RedigoHost = conf.ENV.Redigo.Host
	}
	conn, err := redigo.Dial(NETWORKTCP, RedigoHost)
	if err != nil {
		log.Fatal(err)
	}

	return conn
}

func InjectRedigoHost(host string) {
	RedigoHost = host
}
