package sp

import (
	"fmt"
	"reflect"
	"testing"

	spmod "bitbucket.org/w-bt/sharing/app_go/app/models/service_provider"
	redis "bitbucket.org/w-bt/sharing/app_go/db/redis"
)

func TestGetFromRedis(t *testing.T) {
	testGetFromRedisCasePositive(t)
}

func testGetFromRedisCasePositive(t *testing.T) {
	sp := spmod.SP{
		ID:          1,
		GUID:        "guid",
		FullName:    "full_name",
		Email:       "email",
		Gender:      "gender",
		PhoneNumber: "phone_number",
		IsActive:    true,
		Description: "description",
	}
	redis.Del(fmt.Sprintf(SP_REDIS_KEY, sp.GUID))

	s := &ShowService{
		guid: sp.GUID,
	}

	s.SetToRedis(sp)

	result, err := s.GetFromRedis()
	if err != nil {
		t.Errorf("fail to get data from redis err:%+v\n", err)
	}

	if !reflect.DeepEqual(result, sp) {
		t.Errorf("result are not equal result:%+v \n%+v\n", result, sp)
	}
}
