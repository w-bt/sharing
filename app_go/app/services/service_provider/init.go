package sp

import (
	"github.com/golang/groupcache/singleflight"

	modelsp "bitbucket.org/w-bt/sharing/app_go/app/models/service_provider"
)

var (
	ServiceProvider = modelsp.ServiceProvider
	group           singleflight.Group
)
