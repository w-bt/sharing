package sp

import (
	"crypto/rand"
	"encoding/hex"
	"log"

	spmod "bitbucket.org/w-bt/sharing/app_go/app/models/service_provider"
	kafkapub "bitbucket.org/w-bt/sharing/app_go/app/services/kafka_publishing"
)

type CreateService struct {
	serviceProvider spmod.SP
}

const (
	SP_SINGLEFLIGHT_KEY = "admin:service_provider:%s"
	SP_REDIS_KEY        = "admin:service_provider:%s"
)

func Create(serviceProvider spmod.SP) (service *CreateService) {
	service = &CreateService{
		serviceProvider: serviceProvider,
	}

	return
}

func (i *CreateService) Call() (err error) {
	i.serviceProvider.GUID = randomHex(4)
	err = ServiceProvider().Creation(&i.serviceProvider)
	if err != nil {
		log.Printf("fail performing insert query :%+v\n", err)
		return err
	}

	go kafkapub.Publish(i.serviceProvider)

	return
}

func randomHex(n int) string {
	bytes := make([]byte, n)
	rand.Read(bytes)
	return hex.EncodeToString(bytes)
}
