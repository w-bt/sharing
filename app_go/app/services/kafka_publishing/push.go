package kafkapub

import (
	"log"

	kafkaPKG "bitbucket.org/w-bt/sharing/app_go/config/kafka"
	"github.com/Shopify/sarama"
)

func Push(topic, key, value string) (err error) {
	msg := &sarama.ProducerMessage{
		Topic: topic,
		Key:   sarama.StringEncoder(key),
		Value: sarama.StringEncoder(value),
	}

	producer, err := kafkaPKG.Producer()
	if err != nil {
		log.Printf("failed to create producer %+v", err)
		return
	}
	defer producer.Close()

	partition, offset, err := producer.SendMessage(msg)
	if err != nil {
		log.Printf("failed to send message %+v", err)
		return
	}

	log.Printf("Message is stored in topic(%s)/partition(%d)/offset(%d)\n", topic, partition, offset)

	return nil
}
