syntax = "proto3";

package gojek.esb.booking;

option java_multiple_files = true;
option java_package = "com.gojek.esb.booking";
option java_outer_classname = "GoTixBookingLogProto";
option go_package = "source.golabs.io/hermes/go-esb-log-entities/go-tix-booking";

import "gojek/esb/audit/Audit.proto";
import "gojek/esb/types/Location.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/duration.proto";
import "gojek/esb/types/ServiceType.proto";
import "gojek/esb/types/BookingStatus.proto";
import "gojek/esb/types/CancelSource.proto";
import "gojek/esb/types/CustomerType.proto";
import "gojek/esb/types/PaymentType.proto";
import "gojek/esb/types/Route.proto";
import "gojek/esb/types/VehicleType.proto";
import "gojek/esb/types/CompletionSource.proto";
import "gojek/esb/types/GoTixCategory.proto";

message GoTixBookingLogKey {
    gojek.esb.types.ServiceType.Enum service_type = 1; //required
    string order_number = 2; //required
    string order_url = 3; //required
    gojek.esb.types.BookingStatus.Enum status = 4; //required
    google.protobuf.Timestamp event_timestamp = 5; //required
    gojek.esb.audit.Audit audit = 500;
}

message GoTixBookingLogMessage {
    gojek.esb.types.ServiceType.Enum service_type = 1; //required
    string order_number = 2; //required
    string order_url = 3; //required
    gojek.esb.types.BookingStatus.Enum status = 4; //required
    google.protobuf.Timestamp event_timestamp = 5; //required
    string customer_id = 6; //required
    string customer_url = 7; //required
    string driver_id = 8; //filled during driver_found, cancelled, customer_picked_up, customer_dropped_off, feedback_for_driver, and refunded event_names
    string driver_url = 9;
    bool is_reblast = 10; //if this is a reblasted booking, then true

    string activity_source = 11;
    string service_area_id = 12;

    //payment
    gojek.esb.types.PaymentType.Enum payment_type = 13;
    float total_unsubsidised_price = 14;
    float customer_price = 15;
    float amount_paid_by_cash = 16;
    float amount_paid_by_credits = 17;
    float surcharge_amount = 18;
    float tip_amount = 19;
    float driver_cut_amount = 20;

    //distance, locations
    float total_distance_in_kms = 21;
    string route_polyline = 22;
    repeated gojek.esb.types.Route routes = 23;
    google.protobuf.Duration driver_eta_pickup = 24;
    google.protobuf.Duration driver_eta_dropoff = 25;
    gojek.esb.types.Location driver_pickup_location = 26;
    gojek.esb.types.Location driver_dropoff_location = 27;

    //customer
    string customer_email = 28;
    string customer_name = 29;
    string customer_phone = 30;

    //driver
    string driver_email = 31;
    string driver_name = 32;
    string driver_phone = 33;
    string driver_phone2 = 34;
    string driver_phone3 = 35;

    //cancellation
    int32 cancel_reason_id = 36;
    string cancel_reason_description = 37;
    gojek.esb.types.CancelSource.Enum cancel_source = 38;
    gojek.esb.types.CustomerType.Enum customer_type = 39;

    //booking creation time
    google.protobuf.Timestamp booking_creation_time = 41;

    //customer discount
    float total_customer_discount = 40;
    float gopay_customer_discount = 42;
    float voucher_customer_discount = 43;

    google.protobuf.Timestamp pickup_time = 44;
    float driver_paid_in_cash = 45;
    float driver_paid_in_credit = 46;
    reserved 47;
    reserved "previous_status";
    string receiver_name = 48;
    string driver_photo_url = 49;
    gojek.esb.types.BookingStatus.Enum previous_booking_status = 50;

    gojek.esb.types.VehicleType.Enum vehicle_type = 51;

    // Customer surge information
    int64 customer_total_fare_without_surge = 52;
    float customer_surge_factor = 53;
    int64 customer_dynamic_surge = 54;
    bool customer_dynamic_surge_enabled = 55;

    // Driver surge information
    int64 driver_total_fare_without_surge = 56;
    float driver_surge_factor = 57;
    int64 driver_dynamic_surge = 58;
    bool driver_dynamic_surge_enabled = 59;

    //Actual Time arrival
    google.protobuf.Duration driver_ata_pickup = 60; //actual time arrival
    google.protobuf.Duration driver_ata_dropoff = 61; //actual time arrival

    //Activity Source Information
    string gcm_key = 62; //gcm_key should be filled if activity_source is android
    string device_token = 63; //device_token should be filled if activity_source is ios

    //Financial Information
    string pricing_service_id = 64;
    string payment_invoice_number = 65; //for gopay reconciliation
    string pricing_id = 66; //for uniquely identifying price card of a booking.

    gojek.esb.types.CompletionSource.Enum completion_source = 67; //identify if booking was completed via dispatch or driver app

    float cash_customer_discount = 68; // customer discount for cash payment method

    // Edited booking information
    int64 destination_edited_count = 69;

    // Pricing Currency
    string pricing_currency = 70;

    //driver ID in case of booking reblast
    string previous_driver_id = 71;

    // toll_amount for this booking, it is an integer because we store the subunits (cents, paisa etc)
    // Indonesia is a special case where instead of 'sen', we store rupiyah
    int64 toll_amount = 72;

    // gotix specific information
    gojek.esb.types.GoTixCategory.Enum category = 200;
    int64 event_id = 201;
    string event_name = 202;
    gojek.esb.types.Location venue = 203;
    string ticket_class = 204;
    int32 num_tickets = 205;
    float single_ticket_price = 206;
    float total_ticket_price = 207;
    float convenience_fee = 208;
    float admin_fee = 209;
    string voucher_code = 210;
    float voucher_value = 211;
    bool is_point_voucher = 212;
    int64 parent_order_id = 213;
    string customer_first_name = 214;
    string customer_last_name = 215;
    string phone = 216;
    string email = 217;
    google.protobuf.Timestamp dob = 218;
    int32 gender = 219;
    int64 transaction_fee = 220;
    int64 total_price = 221;
    int64 cc_total_price = 222;
    int64 payment_fee = 223;
    int64 payment_total = 224;
    int64 paid_amount = 225;
    float net_cc_surcharge = 226;
    string booking_reference = 227;
    string external_reference_code = 228;
    int64 status_name = 229;
    int32 is_ticket_available = 230;
    google.protobuf.Timestamp email_sent_at = 231;
    google.protobuf.Timestamp order_date = 232;
    int32 payment_id = 233;
    string payment_name = 234;
    google.protobuf.Timestamp payment_date = 235;
    google.protobuf.Timestamp delivery_date = 236;
    google.protobuf.Timestamp booking_expiry_time = 237;
    string refund_reason = 238;
    string failure_acknowledged = 239;
    string veritrans_token = 240;
    google.protobuf.Timestamp created_at = 241;
    google.protobuf.Timestamp updated_at = 242;
    string passkey = 243;
    string cancel_booking_reference = 244;
    int32 is_points_voucher = 245;
    string voucher_id = 246;
    string wallet_id = 247;
    int64 voucher_amount = 248;
    int64 excess_voucher = 249;
    string platform = 250;
    float cc_surcharge_customer = 251;
    float cc_surcharge_bank = 252;
    float base_price = 253;
    float commision = 254;
    float tax = 255;
    float cc_surcharge_mt = 256;
    string type_of = 257;
    int64 location_id = 258;
    google.protobuf.Timestamp start_date_time = 259;
    google.protobuf.Timestamp finish_date_time = 260;
    string external_schedule_code = 261;
    string auditype = 262;
    string external_provider_code = 263;
    string external_cinema_code = 264;
    string external_showdate = 265;
    string external_showtime = 266;
    string schedule_class = 267;
    string budget_from = 268;
    string payment_identifier = 269;
    string external_order_code = 270;
}
