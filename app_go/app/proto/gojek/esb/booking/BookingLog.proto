syntax = "proto3";

package gojek.esb.booking;

option java_multiple_files = true;
option java_package = "com.gojek.esb.booking";
option java_outer_classname = "BookingLogProto";

option go_package="source.golabs.io/hermes/go-esb-log-entities/booking";

import "gojek/esb/audit/Audit.proto";
import "gojek/esb/types/Location.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/duration.proto";
import "gojek/esb/types/ServiceType.proto";
import "gojek/esb/types/BookingStatus.proto";
import "gojek/esb/types/CancelSource.proto";
import "gojek/esb/types/CustomerType.proto";
import "gojek/esb/types/PaymentType.proto";
import "gojek/esb/types/Route.proto";
import "gojek/esb/types/VehicleType.proto";
import "gojek/esb/types/CompletionSource.proto";
import "gojek/esb/types/PaymentOptionType.proto";
import "gojek/esb/booking/info/BookingInfo.proto";

message BookingLogKey {
    gojek.esb.types.ServiceType.Enum service_type = 1; //required
    string order_number = 2; //required
    string order_url = 3; //required
    gojek.esb.types.BookingStatus.Enum status = 4; //required
    google.protobuf.Timestamp event_timestamp = 5; //required
    gojek.esb.audit.Audit audit = 500;
}

message BookingLogMessage {
    gojek.esb.types.ServiceType.Enum service_type = 1; //required
    string order_number = 2; //required
    string order_url = 3; //required
    gojek.esb.types.BookingStatus.Enum status = 4; //required
    google.protobuf.Timestamp event_timestamp = 5; //required
    string customer_id = 6; //required
    string customer_url = 7; //required
    string driver_id = 8; //filled during driver_found, cancelled, customer_picked_up, customer_dropped_off, feedback_for_driver, and refunded event_names
    string driver_url = 9;
    bool is_reblast = 10; //if this is a reblasted booking, then true

    string activity_source = 11;
    string service_area_id = 12;

    //payment
    gojek.esb.types.PaymentType.Enum payment_type = 13;
    float total_unsubsidised_price = 14;
    float customer_price = 15;
    float amount_paid_by_cash = 16;
    float amount_paid_by_credits = 17;
    float surcharge_amount = 18;
    float tip_amount = 19;
    float driver_cut_amount = 20;

    //distance, locations
    float total_distance_in_kms = 21;
    string route_polyline = 22;
    repeated gojek.esb.types.Route routes = 23;
    google.protobuf.Duration driver_eta_pickup = 24;
    google.protobuf.Duration driver_eta_dropoff = 25;
    gojek.esb.types.Location driver_pickup_location = 26;
    gojek.esb.types.Location driver_dropoff_location = 27;

    //customer
    string customer_email = 28;
    string customer_name = 29;
    string customer_phone = 30;

    //driver
    string driver_email = 31;
    string driver_name = 32;
    string driver_phone = 33;
    string driver_phone2 = 34;
    string driver_phone3 = 35;

    //cancellation
    int32 cancel_reason_id = 36;
    string cancel_reason_description = 37;
    gojek.esb.types.CancelSource.Enum cancel_source = 38;
    gojek.esb.types.CustomerType.Enum customer_type = 39;

    //booking creation time
    google.protobuf.Timestamp booking_creation_time = 41;

    //customer discount
    float total_customer_discount = 40;
    float gopay_customer_discount = 42;
    float voucher_customer_discount = 43;

    google.protobuf.Timestamp pickup_time = 44;
    float driver_paid_in_cash = 45;
    float driver_paid_in_credit = 46;
    reserved 47;
    reserved "previous_status";
    string receiver_name = 48;
    string driver_photo_url = 49;
    gojek.esb.types.BookingStatus.Enum previous_booking_status = 50;

    gojek.esb.types.VehicleType.Enum vehicle_type = 51;

    // Customer surge information
    int64 customer_total_fare_without_surge = 52;
    float customer_surge_factor = 53;
    int64 customer_dynamic_surge = 54;
    bool customer_dynamic_surge_enabled = 55;

    // Driver surge information
    int64 driver_total_fare_without_surge = 56;
    float driver_surge_factor = 57;
    int64 driver_dynamic_surge = 58;
    bool driver_dynamic_surge_enabled = 59;

    //Actual Time arrival
    google.protobuf.Duration driver_ata_pickup = 60; //actual time arrival
    google.protobuf.Duration driver_ata_dropoff = 61; //actual time arrival

    //Activity Source Information
    string gcm_key = 62; //gcm_key should be filled if activity_source is android
    string device_token = 63; //device_token should be filled if activity_source is ios

    //Financial Information
    string pricing_service_id = 64;
    string payment_invoice_number = 65; //for gopay reconciliation
    string pricing_id = 66; //for uniquely identifying price card of a booking.

    gojek.esb.types.CompletionSource.Enum completion_source = 67; //identify if booking was completed via dispatch or driver app

    float cash_customer_discount = 68; // customer discount for cash payment method

    // Edited booking information
    int64 destination_edited_count = 69;

    // Pricing Currency
    string pricing_currency = 70;

    //driver ID in case of booking reblast
    string previous_driver_id = 71;

    // toll_amount for this booking, it is an integer because we store the subunits (cents, paisa etc)
    // Indonesia is a special case where instead of 'sen', we store rupiyah
    int64 toll_amount = 72;

    int64 meter_amount = 73;    // Meter amount that given from bird's driver

    bool is_payment_method_changed = 74;    // flag for changing payment method from system (ex: Ride Service)
    string country_code = 75;   // Follows ISO 3166-2 Standards (https://en.wikipedia.org/wiki/ISO_3166-2)
    string service_area_tzname = 76; //Follows TZ Database Standards (https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
    int64 cancellation_fee_amount = 77;
    gojek.esb.types.Location driver_arrived_location = 78;

    //payment option
    string payment_option_id = 79;
    string payment_option_name = 83;
    gojek.esb.types.PaymentOptionType.Enum payment_option_type = 80;
    PaymentOptionMetadata payment_option_metadata = 81;

    google.protobuf.Timestamp arrival_at_pickup_time = 82;

    //managed queue
    string allocation_strategy = 84;

    // cashback
    int64 cashback_amount = 85;

    string payment_token = 86;
    string payment_intent = 87;

    // route type, Google, straight line etc
    string route_type = 88;

    // voucher id
    string voucher_id = 89;

    //please do not use
    string reserved_amount = 90;

    // Data which is common for orders
    BookingInfo booking_info = 110;

    // NO MORE CHANGES ARE ALLOWED!!
}

message PaymentOptionMetadata {
    string masked_card = 1;
    string network = 2;
}
