package admin

import (
	"context"
	"database/sql"
	"log"
	"net/http"
	"strconv"
	"time"

	controller "bitbucket.org/w-bt/sharing/app_go/app/controllers"
	spmod "bitbucket.org/w-bt/sharing/app_go/app/models/service_provider"
	proto "bitbucket.org/w-bt/sharing/app_go/app/proto"
	serializer "bitbucket.org/w-bt/sharing/app_go/app/services/serializers"
	sp "bitbucket.org/w-bt/sharing/app_go/app/services/service_provider"
	"github.com/julienschmidt/httprouter"
	"google.golang.org/grpc"
)

const (
	DEFAULT_LIMIT = 20
	DEFAULT_SKIP  = 0
)

func HandlerServiceProviderIndex(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	start := time.Now()

	params, err := parseIndexParameters(r)
	if err != nil {
		log.Printf("fail parsing parameter :%+v\n", err)
		controller.Render(w, "", err, http.StatusBadRequest, nil, time.Since(start).String())
		return
	}

	var response []spmod.SP
	response, err = sp.Index(params["limit"], params["skip"]).Call()
	if err != nil {
		log.Printf("fail performing service provider response :%+v\n", err)
		controller.Render(w, "", err, http.StatusInternalServerError, nil, time.Since(start).String())
		return
	}

	var result []serializer.ServiceProviderForAdmin
	for _, resp := range response {
		result = append(result, serializer.ServiceProviderForAdmin(resp))
	}

	controller.Render(w, "", nil, http.StatusOK, result, time.Since(start).String())
}

func parseIndexParameters(r *http.Request) (params map[string]int, err error) {
	params = make(map[string]int)

	rawLimit := r.FormValue("limit")
	params["limit"] = DEFAULT_LIMIT
	if rawLimit != "" {
		params["limit"], err = strconv.Atoi(rawLimit)
		if err != nil {
			log.Printf("fail performing Atoi :%+v\n", err)
			return
		}
	}

	rawSkip := r.FormValue("skip")
	params["skip"] = DEFAULT_SKIP
	if rawSkip != "" {
		params["skip"], err = strconv.Atoi(rawSkip)
		if err != nil {
			log.Printf("fail performing Atoi :%+v\n", err)
			return
		}
	}

	return
}

func HandlerServiceProviderShowV1(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	start := time.Now()

	guid := p.ByName("guid")

	response, err := sp.Show(guid).Call()
	if err != nil {
		log.Printf("fail performing service provider response :%+v\n", err)

		status := http.StatusBadRequest
		if err == sql.ErrNoRows {
			status = http.StatusNotFound
		}
		controller.Render(w, "", err, status, nil, time.Since(start).String())
		return
	}

	controller.Render(w, "", nil, http.StatusOK, response, time.Since(start).String())
}

func HandlerServiceProviderShowV2(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	start := time.Now()

	guid := p.ByName("guid")

	conn, err := grpc.Dial("127.0.0.1:60000", grpc.WithInsecure())
	if err != nil {
		log.Printf("grpc connection failed: %v", err)

		controller.Render(w, "", err, http.StatusInternalServerError, nil, time.Since(start).String())
		return
	}

	client := proto.NewServiceClient(conn)

	resp, err := client.GetServiceProvider(context.Background(), &proto.Request{
		Guid: guid,
	})

	if err != nil || resp == nil {
		log.Printf("grpc request failed: %v", err)

		controller.Render(w, "", err, http.StatusInternalServerError, nil, time.Since(start).String())
	}

	controller.Render(w, "", nil, http.StatusOK, resp, time.Since(start).String())
}

func HandlerServiceProviderCreation(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	start := time.Now()

	spObject, err := parseCreationParameters(r)
	if err != nil {
		log.Printf("fail parsing creation param :%+v\n", err)
		return
	}

	err = sp.Create(spObject).Call()
	if err != nil {
		log.Printf("fail performing service provider creation :%+v\n", err)
		controller.Render(w, "", err, http.StatusInternalServerError, nil, time.Since(start).String())
		return
	}

	controller.Render(w, "", nil, http.StatusOK, nil, time.Since(start).String())
}

func parseCreationParameters(r *http.Request) (spObject spmod.SP, err error) {
	spObject.FullName = r.FormValue("full_name")
	spObject.Email = r.FormValue("email")
	spObject.Gender = r.FormValue("gender")
	spObject.PhoneNumber = r.FormValue("phone_number")

	rawIsActive := r.FormValue("is_active")
	spObject.IsActive, err = strconv.ParseBool(rawIsActive)
	if err != nil {
		log.Printf("fail performing ParseBool :%+v\n", err)
		return
	}

	spObject.Description = r.FormValue("description")

	return
}
