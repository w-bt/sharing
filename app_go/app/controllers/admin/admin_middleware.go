package admin

import (
	"encoding/base64"
	"net/http"
	"strings"

	conf "bitbucket.org/w-bt/sharing/app_go/config/environments"
	"github.com/julienschmidt/httprouter"
)

func Authenticate(next httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		auth := strings.SplitN(r.Header.Get("Authorization"), " ", 2)

		if len(auth) != 2 || auth[0] != "Basic" {
			http.Error(w, "authorization failed", http.StatusUnauthorized)
			return
		}

		payload, _ := base64.StdEncoding.DecodeString(auth[1])
		pair := strings.SplitN(string(payload), ":", 2)

		if len(pair) != 2 || !validate(pair[0], pair[1]) {
			http.Error(w, "authorization failed", http.StatusUnauthorized)
			return
		}

		next(w, r, p)
	}
}

func validate(username, password string) bool {
	if username == conf.ENV.Admin.ClientID && password == conf.ENV.Admin.SecretKey {
		return true
	}
	return false
}
