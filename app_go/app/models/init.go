package model

import (
	spmod "bitbucket.org/w-bt/sharing/app_go/app/models/service_provider"
)

func Init() {
	spmod.Init()
}
