package spmod

import (
	"database/sql"
	"log"
	"time"

	"bitbucket.org/w-bt/sharing/app_go/db"
)

type ServiceProviderModel struct {
	db      *sql.DB
	Queries *Queries
}

type Queries struct {
	GetServiceProviderList     *sql.Stmt
	GetServiceProvider         *sql.Stmt
	GetInactiveServiceProvider *sql.Stmt
}

type SP struct {
	ID          int64
	GUID        string
	FullName    string
	Email       string
	Gender      string
	PhoneNumber string
	IsActive    bool
	Description string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

var spModel *ServiceProviderModel

func Init() {
	spModel = &ServiceProviderModel{
		db: db.Get(),
	}
	spModel.PrepareQueries()
}

func ServiceProvider() *ServiceProviderModel {
	if spModel == nil {
		log.Printf("failed to get service provider\n")
		return nil
	}

	return spModel
}

func (s *ServiceProviderModel) PrepareQueries() {
	s.Queries = &Queries{
		GetServiceProviderList:     prepare(getServiceProviderList, s.db),
		GetServiceProvider:         prepare(getServiceProvider, s.db),
		GetInactiveServiceProvider: prepare(getInactiveServiceProvider, s.db),
	}
}

func (s *ServiceProviderModel) Index(limit int, skip int) (spList []SP, err error) {
	var sp SP

	rows, err := s.Queries.GetServiceProviderList.Query(limit, skip)
	if err != nil {
		log.Printf("fail performing select query :%+v\n", err)
		return spList, err
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(
			&sp.ID,
			&sp.GUID,
			&sp.FullName,
			&sp.Email,
			&sp.Gender,
			&sp.PhoneNumber,
			&sp.IsActive,
			&sp.Description,
			&sp.CreatedAt,
			&sp.UpdatedAt,
		)
		if err != nil {
			log.Printf("fail performing scan :%+v\n", err)
			continue
		}

		spList = append(spList, sp)
	}

	if err = rows.Err(); err != nil {
		log.Printf("error rows :%+v\n", err)
		return spList, err
	}

	return
}

func (s *ServiceProviderModel) Show(guid string) (sp SP, err error) {
	err = s.Queries.GetServiceProvider.QueryRow(guid).Scan(
		&sp.ID,
		&sp.GUID,
		&sp.FullName,
		&sp.Email,
		&sp.Gender,
		&sp.PhoneNumber,
		&sp.IsActive,
		&sp.Description,
		&sp.CreatedAt,
		&sp.UpdatedAt,
	)

	if err != nil {
		log.Printf("fail performing select query :%+v\n", err)
		return sp, err
	}

	return
}

func (s *ServiceProviderModel) Creation(sp *SP) (err error) {
	err = s.db.QueryRow(
		insertServiceProvider,
		sp.GUID,
		sp.FullName,
		sp.Gender,
		sp.Email,
		sp.PhoneNumber,
		sp.IsActive,
		sp.Description,
	).Scan(
		&sp.ID,
		&sp.CreatedAt,
		&sp.UpdatedAt,
	)
	if err != nil {
		log.Printf("failed to insert sp:%+v, err:%+v\n", sp, err)
	}

	return
}

func (s *ServiceProviderModel) Activation(spID int64) (err error) {
	_, err = s.db.Exec(updateIsActiveServiceProvider, spID)
	if err != nil {
		log.Printf("failed to insert sp:%+v, err:%+v\n", spID, err)
	}

	return
}

func (s *ServiceProviderModel) InactiveList() (spList []SP, err error) {
	var sp SP

	rows, err := s.Queries.GetInactiveServiceProvider.Query()
	if err != nil {
		log.Printf("fail performing select query :%+v\n", err)
		return spList, err
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(
			&sp.ID,
			&sp.GUID,
			&sp.FullName,
			&sp.Email,
			&sp.Gender,
			&sp.PhoneNumber,
			&sp.IsActive,
			&sp.Description,
			&sp.CreatedAt,
			&sp.UpdatedAt,
		)
		if err != nil {
			log.Printf("fail performing scan :%+v\n", err)
			continue
		}

		spList = append(spList, sp)
	}

	if err = rows.Err(); err != nil {
		log.Printf("error rows :%+v\n", err)
		return spList, err
	}

	return
}

func prepare(query string, db *sql.DB) *sql.Stmt {
	s, err := db.Prepare(query)
	if err != nil {
		log.Println("failed to prepare query", query, err)
	}
	return s
}

const (
	getServiceProviderList = `SELECT
			id,
			guid,
			full_name,
			gender,
			email,
			phone_number,
      is_active,
      description,
      created_at,
      updated_at
		FROM service_providers
		ORDER BY created_at DESC
    LIMIT $1 OFFSET $2`

	getServiceProvider = `SELECT
			id,
			guid,
			full_name,
			gender,
			email,
			phone_number,
      is_active,
      description,
      created_at,
      updated_at
		FROM service_providers
		WHERE guid = $1`

	getInactiveServiceProvider = `SELECT
			id,
			guid,
			full_name,
			gender,
			email,
			phone_number,
      is_active,
      description,
      created_at,
      updated_at
		FROM service_providers
		WHERE is_active = false`

	updateIsActiveServiceProvider = `
		UPDATE service_providers
		SET
			is_active = true,
			updated_at = NOW()
		WHERE id = $1
	`

	insertServiceProvider = `
		INSERT INTO service_providers(
			guid,
			full_name,
			gender,
			email,
			phone_number,
      is_active,
      description,
      created_at,
      updated_at
		)
		VALUES
			($1, $2, $3, $4, $5, $6, $7, NOW(), NOW())
		RETURNING
			id,
			created_at,
			updated_at`
)
