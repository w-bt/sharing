package spmod

import (
	"database/sql"
	"errors"
	"os"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
)

var (
	mockDB *sql.DB
	mock   sqlmock.Sqlmock
)

const (
	RGXSPLIST     = "SELECT (.+) FROM service_providers ORDER BY created_at DESC LIMIT (.+) OFFSET (.+)"
	RGXSPSHOW     = "SELECT (.+) FROM service_providers WHERE guid = (.+)"
	RGXSPINACTIVE = "SELECT (.+) FROM service_providers WHERE is_active = false"
)

func TestMain(m *testing.M) {
	var err error
	mockDB, mock, err = sqlmock.New()
	if err != nil {
		return
	}

	defer mockDB.Close()
	spModel = &ServiceProviderModel{
		db: mockDB,
	}

	mock.MatchExpectationsInOrder(false)

	mock.ExpectPrepare(RGXSPLIST)
	mock.ExpectPrepare(RGXSPSHOW)
	mock.ExpectPrepare(RGXSPINACTIVE)

	spModel.PrepareQueries()

	os.Exit(m.Run())
}

func TestServiceProvider(t *testing.T) {
	testServiceProviderCasePositive(t)
	testServiceProviderCaseNegative(t)
}

func testServiceProviderCasePositive(t *testing.T) {
	model := ServiceProvider()
	if model == nil {
		t.Errorf("failed to test testServiceProviderCasePositive, expected model is not nil")
	}
}

func testServiceProviderCaseNegative(t *testing.T) {
	tempModel := new(ServiceProviderModel)
	*tempModel = *spModel
	spModel = nil
	model := ServiceProvider()
	if model != nil {
		t.Errorf("failed to test testServiceProviderCaseNegative, expected model is be nil")
	}
	spModel = tempModel
}

func TestIndex(t *testing.T) {
	testIndexCasePositive(t)
	testIndexCaseNegativeErrorQuery(t)
	testIndexCaseNegativeErrorScan(t)
	testIndexCaseNegativeErrorRow(t)
}

func testIndexCasePositive(t *testing.T) {
	limit := 1
	skip := 0

	rows := sqlmock.NewRows(
		[]string{
			"id",
			"guid",
			"full_name",
			"gender",
			"email",
			"phone_number",
			"is_active",
			"description",
			"created_at",
			"updated_at",
		}).AddRow(
		1,
		"guid",
		"full_name",
		"male",
		"email",
		"phone_number",
		true,
		"description",
		time.Now(),
		time.Now(),
	)
	mock.ExpectPrepare(RGXSPLIST).ExpectQuery().WithArgs(limit, skip).WillReturnRows(rows)

	_, err := spModel.Index(limit, skip)
	if mock.ExpectationsWereMet(); err != nil {
		t.Errorf("ExpectationsWereMet err:%+v\n", err)
	}
}

func testIndexCaseNegativeErrorQuery(t *testing.T) {
	limit := 1
	skip := 1

	mock.ExpectPrepare(RGXSPLIST).ExpectQuery().WithArgs(limit, skip).WillReturnError(errors.New("error expected"))

	_, err := spModel.Index(limit, skip)
	if mock.ExpectationsWereMet(); err == nil {
		t.Errorf("ExpectationsWereMet err:%+v\n", err)
	}
}

func testIndexCaseNegativeErrorScan(t *testing.T) {
	limit := 1
	skip := 2

	rows := sqlmock.NewRows(
		[]string{
			"is_active",
		}).AddRow(
		"error expected",
	)
	mock.ExpectPrepare(RGXSPLIST).ExpectQuery().WithArgs(limit, skip).WillReturnRows(rows)

	list, err := spModel.Index(limit, skip)
	if mock.ExpectationsWereMet(); err != nil || len(list) > 0 {
		t.Errorf("ExpectationsWereMet err:%+v\n", err)
	}
}

func testIndexCaseNegativeErrorRow(t *testing.T) {
	limit := 1
	skip := 3

	rows := sqlmock.NewRows(
		[]string{
			"id",
			"guid",
			"full_name",
			"gender",
			"email",
			"phone_number",
			"is_active",
			"description",
			"created_at",
			"updated_at",
		}).AddRow(
		1,
		"guid",
		"full_name",
		"male",
		"email",
		"phone_number",
		true,
		"description",
		time.Now(),
		time.Now(),
	).RowError(0, errors.New("expected error"))
	mock.ExpectPrepare(RGXSPLIST).ExpectQuery().WithArgs(limit, skip).WillReturnRows(rows)

	_, err := spModel.Index(limit, skip)
	if mock.ExpectationsWereMet(); err == nil {
		t.Errorf("ExpectationsWereMet err:%+v\n", err)
	}
}
