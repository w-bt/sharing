package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	booking "bitbucket.org/w-bt/sharing/app_go/app/proto/source.golabs.io/hermes/go-esb-log-entities/go-life-booking"
	"github.com/Shopify/sarama"
	"github.com/golang/protobuf/proto"
)

func main() {

	version, err := sarama.ParseKafkaVersion("2.1.1")
	if err != nil {
		log.Panicf("Error parsing Kafka version: %v", err)
	}

	/**
	 * Construct a new Sarama configuration.
	 * The Kafka cluster version has to be defined before the consumer/producer is initialized.
	 */
	config := sarama.NewConfig()
	config.Version = version
	config.Consumer.Return.Errors = true
	config.Consumer.Offsets.Initial = sarama.OffsetOldest

	// Specify brokers address. This is default one
	brokers := []string{
		"p-golife-v3-kafka-app-a-01:6667",
		"p-golife-v3-kafka-app-a-02:6667",
		"p-golife-v3-kafka-app-a-03:6667",
		"p-golife-v3-kafka-app-a-04:6667",
		"p-golife-v3-kafka-app-a-05:6667",
	}

	group := "dashboard-analytics"

	topics := []string{"GO_LIFE-booking-log"}

	consumer := Consumer{
		ready: make(chan bool),
	}

	ctx, cancel := context.WithCancel(context.Background())
	client, err := sarama.NewConsumerGroup(brokers, group, config)
	if err != nil {
		log.Panicf("Error creating consumer group client: %v", err)
	}

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			if err := client.Consume(ctx, topics, &consumer); err != nil {
				log.Panicf("Error from consumer: %v", err)
			}
			// check if context was cancelled, signaling that the consumer should stop
			if ctx.Err() != nil {
				return
			}
			consumer.ready = make(chan bool)
		}
	}()

	<-consumer.ready // Await till the consumer has been set up
	log.Println("Sarama consumer up and running!...")

	sigterm := make(chan os.Signal, 1)
	signal.Notify(sigterm, syscall.SIGINT, syscall.SIGTERM)
	select {
	case <-ctx.Done():
		log.Println("terminating: context cancelled")
	case <-sigterm:
		log.Println("terminating: via signal")
	}
	cancel()
	wg.Wait()
	if err = client.Close(); err != nil {
		log.Panicf("Error closing client: %v", err)
	}
}

// Consumer represents a Sarama consumer group consumer
type Consumer struct {
	ready chan bool
}

// Setup is run at the beginning of a new session, before ConsumeClaim
func (consumer *Consumer) Setup(sarama.ConsumerGroupSession) error {
	close(consumer.ready)
	return nil
}

// Cleanup is run at the end of a session, once all ConsumeClaim goroutines have exited
func (consumer *Consumer) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

// ConsumeClaim must start a consumer loop of ConsumerGroupClaim's Messages().
func (consumer *Consumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {

	for msg := range claim.Messages() {

		// key := booking.GoLifeBookingLogKey{}
		// proto.Unmarshal(msg.Key, &key)
		message := booking.GoLifeBookingLogMessage{}
		proto.Unmarshal(msg.Key, &message)
		// fmt.Printf("Received key %+v\n", key)
		// fmt.Printf("Received message %+v \n", message)
		fmt.Printf("Booking GUID %+v\n", message.GetOrderNumber())

		session.MarkMessage(msg, "")
	}

	return nil
}
