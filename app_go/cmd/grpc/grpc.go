package main

import (
	"log"
	"net"

	model "bitbucket.org/w-bt/sharing/app_go/app/models"
	proto "bitbucket.org/w-bt/sharing/app_go/app/proto"
	sp "bitbucket.org/w-bt/sharing/app_go/app/services/service_provider"
	conf "bitbucket.org/w-bt/sharing/app_go/config/environments"
	"bitbucket.org/w-bt/sharing/app_go/db"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func init() {
	conf.InitConfig()
	db.Init()
	model.Init()
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Printf("GRPC Server Start\n")

	lis, _ := net.Listen("tcp", ":60000")

	srv := grpc.NewServer()
	proto.RegisterServiceServer(srv, &Server{})
	log.Println(srv.Serve(lis))
}

// Server type
type Server struct{}

// GetServiceProvider handler
func (s *Server) GetServiceProvider(ctx context.Context, in *proto.Request) (*proto.ServiceProvider, error) {
	response, err := sp.Show(in.GetGuid()).Call()
	if err != nil {
		log.Printf("fail performing get service provider err:%+v\n", err)
		return &proto.ServiceProvider{}, err
	}

	return &proto.ServiceProvider{
		Id:          response.ID,
		Guid:        response.GUID,
		FullName:    response.FullName,
		Email:       response.Email,
		Gender:      response.Gender,
		PhoneNumber: response.PhoneNumber,
		IsActive:    response.IsActive,
	}, nil
}
