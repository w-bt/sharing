package main

import (
	"fmt"
	"os"
	"os/signal"

	booking "bitbucket.org/w-bt/sharing/app_go/app/proto/source.golabs.io/hermes/go-esb-log-entities/go-life-booking"
	"github.com/Shopify/sarama"
	proto "github.com/golang/protobuf/proto"
)

func main() {

	config := sarama.NewConfig()
	config.Consumer.Return.Errors = true
	config.Consumer.Offsets.Initial = sarama.OffsetOldest

	// Specify brokers address. This is default one
	brokers := []string{
		"p-golife-v3-kafka-app-a-01:6667",
		"p-golife-v3-kafka-app-a-02:6667",
		"p-golife-v3-kafka-app-a-03:6667",
		"p-golife-v3-kafka-app-a-04:6667",
		"p-golife-v3-kafka-app-a-05:6667",
	}

	// Create new consumer
	master, err := sarama.NewConsumer(brokers, config)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := master.Close(); err != nil {
			panic(err)
		}
	}()

	topic := "GO_LIFE-booking-log"
	// How to decide partition, is it fixed value...?
	consumer, err := master.ConsumePartition(topic, 0, sarama.OffsetOldest)
	if err != nil {
		panic(err)
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	// Count how many message processed
	msgCount := 0

	// Get signnal for finish
	doneCh := make(chan struct{})
	go func() {
		for {
			select {
			case err := <-consumer.Errors():
				fmt.Println(err)
			case msg := <-consumer.Messages():
				msgCount++
				key := booking.GoLifeBookingLogKey{}
				proto.Unmarshal(msg.Key, &key)
				message := booking.GoLifeBookingLogMessage{}
				proto.Unmarshal(msg.Key, &message)
				fmt.Printf("Received key %+v\n", key)
				fmt.Printf("Received message %+v \n", message)
			case <-signals:
				fmt.Println("Interrupt is detected")
				doneCh <- struct{}{}
			}
		}
	}()

	<-doneCh
	fmt.Println("Processed", msgCount, "messages")
}
