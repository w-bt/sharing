package main

import (
	"log"
	"net/http"

	model "bitbucket.org/w-bt/sharing/app_go/app/models"
	"bitbucket.org/w-bt/sharing/app_go/config/cron"
	conf "bitbucket.org/w-bt/sharing/app_go/config/environments"
	"bitbucket.org/w-bt/sharing/app_go/config/kafka"
	"bitbucket.org/w-bt/sharing/app_go/db"
)

func init() {
	conf.InitConfig()
	db.Init()
	model.Init()
	kafka.Init()
}

func main() {
	log.SetFlags(log.LstdFlags | log.Llongfile)

	cronMod := cron.New()
	cronMod.Run()

	go func() {
		for {
			err := <-cronMod.ListenError()
			if err != nil {
				log.Println("err cron stopped", err)
			}

		}
	}()
	defer cronMod.Stop()

	log.Fatal(http.ListenAndServe(":9001", nil))
}
