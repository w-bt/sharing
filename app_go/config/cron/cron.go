package cron

import (
	"errors"
	"fmt"
	"log"

	"github.com/robfig/cron"

	sp "bitbucket.org/w-bt/sharing/app_go/app/services/service_provider"
)

type (
	Cron struct {
		jCron       *cron.Cron
		listenErrCh chan error
		jobs        []Job
	}
	Job struct {
		Name    string
		Rule    string
		Handler func() error
	}
	F func()
)

func New() *Cron {
	c := &Cron{
		jCron:       cron.New(),
		listenErrCh: make(chan error),
	}

	c.Register(Job{
		Name:    "Activate Service Provider",
		Rule:    "@every 10s",
		Handler: sp.Activation().Call,
	})

	return c
}

func (c *Cron) Register(j Job) {
	c.jobs = append(c.jobs, j)
}

func (c *Cron) Run() {
	for _, j := range c.jobs {
		c.jCron.AddFunc(j.Rule, func(j Job) F {
			return func() {
				if j.Rule != "@every 1m" {
					log.Printf("Cron [%s] invoked", j.Name)
				}
				err := j.Handler()
				if err != nil {
					c.listenErrCh <- fmt.Errorf("Cron [%s] Error: %s", j.Name, err.Error())
				}
				log.Printf("Cron [%s] executed", j.Name)
			}
		}(j))
		log.Printf("[Cron] %s registered", j.Name)
	}
	c.jCron.Start()
}

func (c *Cron) ListenError() <-chan error {
	return c.listenErrCh
}

func (c *Cron) Stop() {
	c.jCron.Stop()
	c.listenErrCh <- errors.New("Stop scheduler")
}
