package environment

import (
	"fmt"
	"log"
	"os"

	gcfg "gopkg.in/gcfg.v1"
)

// Config application configuration struct relecting `.ini` file structure
type (
	Config struct {
		Environment string
		Server      struct {
			Port string
		}
		PostgreSQL struct {
			Master      string
			MaxOpenConn int
			MaxIdleConn int
		}
		Redigo struct {
			Host string
		}
		Admin struct {
			ClientID  string
			SecretKey string
		}
		Kafka struct {
			ClientID string
			Host     string
			Topic    string
		}
	}
)

var ENV *Config

// ReadConfig read `*.ini` configuration file and save it to variable of `*Config` type
func InitConfig() {
	var (
		err     error
		environ string
	)

	ENV = new(Config)

	environ = os.Getenv("TKPENV")
	if environ == "" {
		environ = "development"
	}

	path := FilePathList()

	for _, val := range path {
		file := fmt.Sprintf(val, environ)
		err := gcfg.ReadFileInto(ENV, file)
		if err == nil {
			break
		}
	}

	if err != nil {
		log.Fatalf("[app_go] Cannot load config env:%s :%+v\n ", environ, err)
	}

	log.Printf("[app_go] Config load success, using \"%s\".\n", environ)
	ENV.Environment = environ
}

// FilePathList list of possible config file relative path to binary location
func FilePathList() []string {
	return []string{
		"./config/environments/%s.ini",
		"../../config/environments/%s.ini",
	}
}
