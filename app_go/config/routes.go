package config

import (
	"net/http"

	"bitbucket.org/w-bt/sharing/app_go/app/controllers/admin"
	"github.com/julienschmidt/httprouter"
)

func InitRouter(router *httprouter.Router) {
	router.GET("/v1/admin/service_providers", admin.Authenticate(admin.HandlerServiceProviderIndex))
	router.GET("/v1/admin/service_providers/:guid", admin.Authenticate(admin.HandlerServiceProviderShowV1))
	router.GET("/v2/admin/service_providers/:guid", admin.Authenticate(admin.HandlerServiceProviderShowV2))

	router.POST("/v1/admin/service_providers", admin.Authenticate(admin.HandlerServiceProviderCreation))
	router.OPTIONS("/v1/admin/service_providers", HandlerOptions)
}

func HandlerOptions(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("origin"))
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "GET,PATCH,DELETE,OPTIONS,POST,PUT")
	w.Header().Set("Access-Control-Allow-Headers", "Authorization, Content-MD5, Content-Type, X-User-ID, X-Date, X-Device")
	return
}
