package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

func main() {
	//testNoConcurency()
	//testConcurency1()
	//testConcurencyNoChannel()
	testConcurencyChannel()
	//testConcurencyBufferedChannel()
	//testConcurencyChannelDeadLock()
}

func testNoConcurency() {
	start := time.Now()

	for char := 'a'; char < 'a'+26; char++ {
		fmt.Printf("%c ", char)
	}

	time.Sleep(1 * time.Second)
	for number := 1; number < 27; number++ {
		fmt.Printf("%d ", number)
	}

	time.Sleep(2 * time.Second)
	for number := 100; number < 126; number++ {
		fmt.Printf("%d ", number)
	}

	fmt.Println("\nDuration", time.Since(start).String())
}

func testConcurency1() {
	start := time.Now()
	//runtime.GOMAXPROCS(1)

	var wg sync.WaitGroup
	wg.Add(3)

	fmt.Println("Starting Go Routines")
	go func() {
		defer wg.Done()

		for char := 'a'; char < 'a'+26; char++ {
			fmt.Printf("%c ", char)
		}
	}()

	go func() {
		defer wg.Done()
		//time.Sleep(1 * time.Second)
		for number := 1; number < 27; number++ {
			fmt.Printf("%d ", number)
		}
	}()

	go func() {
		defer wg.Done()
		//time.Sleep(2 * time.Second)
		for number := 100; number < 126; number++ {
			fmt.Printf("%d ", number)
		}
	}()

	fmt.Println("Waiting To Finish")
	wg.Wait()

	fmt.Println("\nDuration", time.Since(start).String())
}

func testConcurencyNoChannel() {
	var a, b string
	var wg sync.WaitGroup
	wg.Add(2)

	fmt.Println("Starting Go Routines")
	go func() {
		defer wg.Done()

		a = "hello"
	}()

	go func() {
		defer wg.Done()

		b = "world"
	}()

	fmt.Println("Waiting To Finish")
	wg.Wait()

	fmt.Println("Printing a", a, "b", b)
}

func testConcurencyChannel() {
	ch := make(chan int)

	fmt.Println("Starting Go Routines")
	go func() {
		ch <- 1
	}()

	go func() {
		ch <- 2
	}()

	i := <-ch
	fmt.Println("Result", i)
}

func testConcurencyBufferedChannel() {
	runtime.GOMAXPROCS(1)
	ch := make(chan int, 2)

	go func(ch chan int) {
		for i := 1; i <= 5; i++ {
			ch <- i
			fmt.Println("Func goroutine sends data: ", i)
		}
		close(ch)
	}(ch)

	fmt.Println("Main goroutine sleeps 2 seconds")
	time.Sleep(time.Second * 2)

	fmt.Println("Main goroutine begins receiving data")
	for d := range ch {
		fmt.Println("Main goroutine received data:", d)
	}
}

func testConcurencyChannelDeadLock() {
	runtime.GOMAXPROCS(2)

	messages := make(chan string, 2)

	messages <- "widhian"
	messages <- "widhian1"
	messages <- "widhian2"

	for {
		for i := 0; i < 10; i++ {
			fmt.Print("get ", messages)
		}
	}
}
