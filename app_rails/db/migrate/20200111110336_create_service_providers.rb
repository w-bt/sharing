class CreateServiceProviders < ActiveRecord::Migration[5.0]
  def change
    create_table :service_providers do |t|
      t.string :guid, null: false
      t.string :full_name, null: false
      t.string :gender, null: false
      t.string :email, null: false
      t.string :phone_number, null: false
      t.boolean :is_active, null: false
      t.text :description

      t.timestamps
    end
  end
end
