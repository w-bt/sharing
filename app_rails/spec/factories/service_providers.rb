FactoryBot.define do
  factory :service_provider do
    guid { "SP-#{SecureRandom.hex(4)}" }
    full_name { Faker::Name.name }
    is_active { true }
    email { Faker::Internet.unique.email }
    phone_number { Faker::PhoneNumber.phone_number }
    gender { %w[female male].sample }
    description { Faker::Lorem.paragraph }
  end
end
