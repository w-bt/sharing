class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def merge_errors_from(model)
    model.errors.each do |key, value|
      self.errors.add("#{model.model_name.param_key}.#{key}".to_sym, value)
    end
  end
end
