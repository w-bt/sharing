# frozen_string_literal: true

# :reek:TooManyInstanceVariables

class Response
  SUCCESS = "SUCCESS"
  FAILURE = "FAILURE"

  attr_reader :code, :data, :message, :errors, :time_execution

  def self.from(hash)
    new(hash)
  end

  def self.success(**attrs)
    attrs[:code] ||= SUCCESS
    new(attrs)
  end

  def self.failure(**attrs)
    attrs[:code] ||= FAILURE
    new(attrs)
  end

  def initialize(code: "", data: nil, message: "", errors: [message], time_execution: nil)
    @code = code
    @data = data
    @message = message
    @errors = errors
    @time_execution = time_execution
  end
end
