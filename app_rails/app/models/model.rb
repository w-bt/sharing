module Model
  extend ActiveSupport::Concern
  include ActiveModel::Model

  class_methods do
    def attribute(name, options = nil)
      @attributes << name
      attr_accessor name
      validates name, options if options
    end

    def attributes
      @attributes
    end
  end

  included do
    @attributes = []
    def attributes
      self.class.attributes.map do |attribute|
        [attribute, send(attribute)]
      end.to_h
    end
  end
end
