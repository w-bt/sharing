class ServiceProvider < ApplicationRecord
  validates :guid, :full_name, :phone_number, :email, presence: true
  validates :guid, :email, uniqueness: true
  validates :phone_number, uniqueness: true, case_sensitive: false
  validates :gender, inclusion: { in: %w[female male] }
  validates :is_active, inclusion: [true, false]
  attribute :is_active, :boolean, default: false
end
