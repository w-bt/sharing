class KafkaPublishingWorker
  include Sidekiq::Worker
  sidekiq_options(queue: :kafka_publishing)

  def perform(base_encoded_message, base_encoded_key, params = {})
    params.symbolize_keys!

    message = Base64.decode64(base_encoded_message)
    key = Base64.decode64(base_encoded_key)
    topic = params[:topic]
    DeliveryBoy.deliver(message, key: key, topic: topic)
  end
end
