class ServiceProviderActivationWorker
  include Sidekiq::Worker
  sidekiq_options queue: "sp_activation"

  def perform
    ServiceProviderActivation.new.call
  end
end
