class Unauthorized < StandardError
  def initialize(message = nil)
    super(message || I18n.t("errors.messages.unauthorized"))
  end
end
