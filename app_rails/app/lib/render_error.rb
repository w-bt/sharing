module RenderError
  extend ActiveSupport::Concern

  def render_error(status, message)
    body = ::Response.failure(message: message).to_json
    headers = {
      "Content-Type" => "application/json",
      "Content-Length" => "#{body.bytesize}"
    }
    [Rack::Utils::SYMBOL_TO_STATUS_CODE[status] || 500, headers, [body]]
  end
end
