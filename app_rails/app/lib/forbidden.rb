class Forbidden < StandardError
  def initialize(message = nil)
    super(message || I18n.t("errors.messages.forbidden"))
  end
end
