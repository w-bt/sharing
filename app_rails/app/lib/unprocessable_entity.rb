class UnprocessableEntity < StandardError
  def initialize(message = nil)
    super(message || I18n.t("errors.messages.unprocessable_entity"))
  end
end
