class NotFound < StandardError
  def initialize(message = nil)
    super(message || I18n.t("errors.messages.not_found"))
  end
end
