class BadRequest < StandardError
  def initialize(message = nil)
    super(message || I18n.t("errors.messages.bad_request"))
  end
end
