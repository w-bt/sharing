class ActionTaken < UnprocessableEntity
  def initialize
    super(I18n.t("errors.messages.action_taken"))
  end
end
