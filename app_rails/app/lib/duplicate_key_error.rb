class DuplicateKeyError < StandardError
  def initialize
    super(I18n.t("errors.messages.duplicate_key_error"))
  end
end
