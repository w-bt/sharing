class ApplicationController < ActionController::API
  before_action :set_locale

  def ping
    head :ok
  end

  def health
    head :ok
  end

  def route_error
    error_message = I18n.t("errors.messages.route_error", url: request.original_url)
    render_error(:not_found, error_message)
  end

  def set_locale
    I18n.locale = :id
  end

  # https://apidock.com/rails/ActiveSupport/Rescuable/ClassMethods/rescue_from
  # exception handlers are searched from right to left, bottom to top, and up
  # the hierarchy!
  rescue_from StandardError,
              HTTPService::Error,
              with: :internal_server_error

  rescue_from ActiveRecord::RecordInvalid,
              with: :record_invalid

  rescue_from ActionController::ParameterMissing,
              with: :parameter_missing

  rescue_from ActiveModel::ValidationError,
              UnprocessableEntity,
              with: :unprocessable_entity

  rescue_from ActiveRecord::RecordNotFound,
              NotFound,
              with: :not_found

  rescue_from Forbidden,
              with: :forbidden

  rescue_from Unauthorized,
              with: :unauthorized

  rescue_from ActiveRecord::RecordNotUnique, with: :record_not_unique

  rescue_from BadRequest, with: :bad_request

  private
    def serialize(resource, options = {})
      ActiveModelSerializers::SerializableResource.new(resource, options)
    end

    def internal_server_error(error)
      render_error(:internal_server_error, error.message)
    end

    def parameter_missing(error)
      render_error(:bad_request, I18n.t("errors.messages.parameter_missing", param: error.param))
    end

    def record_invalid(error)
      render_error(:bad_request, error.message, errors: error.record.errors.full_messages)
    end

    def unprocessable_entity(error)
      render_error(:unprocessable_entity, error.message)
    end

    def not_found(error)
      render_error(:not_found, I18n.t("errors.messages.not_found", model: error.model))
    end

    def forbidden(error)
      render_error(:forbidden, error.message)
    end

    def unauthorized(error)
      render_error(:unauthorized, error.message)
    end

    def record_not_unique(error)
      render_error(:bad_request, error.message)
    end

    def bad_request(error)
      render_error(:bad_request, error.message)
    end

    def render_error(status, message, errors: [message])
      render status: status, json: Response.failure(message: message, errors: errors)
    end
end
