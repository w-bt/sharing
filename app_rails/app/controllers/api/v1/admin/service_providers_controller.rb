class Api::V1::Admin::ServiceProvidersController < Api::V1::AdminController
  def index
    start = Time.now
    service = ::Admin::ServiceProviders::IndexService
    response = service.new(limit, skip).call
    render status: :ok, json: Response.success(data: response, time_execution: (Time.now - start).in_milliseconds)
  end

  def show
    start = Time.now
    service = ::Admin::ServiceProviders::ShowService
    response = service.new(service_provider_guid).call
    render status: :ok, json: Response.success(data: response, time_execution: (Time.now - start).in_milliseconds)
  end

  def create
    start = Time.now
    service = ::Admin::ServiceProviders::CreateService
    service.new(create_params).call
    render status: :ok, json: Response.success(time_execution: (Time.now - start).in_milliseconds)
  end

  private

    DEFAULT_LIMIT = 20
    DEFAULT_SKIP = 0

    def limit
      params[:limit] || DEFAULT_LIMIT
    end

    def skip
      params[:skip] || DEFAULT_SKIP
    end

    def service_provider_guid
      params[:id]
    end

    def create_params
      params.permit(
        :full_name,
        :gender,
        :email,
        :phone_number,
        :is_active,
        :description
      ).merge(guid: "SP-#{SecureRandom.hex(4)}")
    end
end
