class Api::V1::AdminController < ApplicationController
  include ActionController::HttpAuthentication::Basic::ControllerMethods

  before_action :authenticate

  private
    def authenticate
      authenticate_with_http_basic do |username, password|
        return if username == ENV["CLIENT_ID"] && password == ENV["SECRET_KEY"]
      end
      raise Unauthorized
    end
end
