require 'faraday'
require 'circuitbox/faraday_middleware'

class HTTPService
  class Error < StandardError
    attr_reader :status
    def initialize(status, message, context = {})
      super(message)
      @status = status
      @context = context
    end

    def not_found?
      status == 404
    end

    def raven_context
      @context
    end
  end

  class <<self
    def http(options = {})
      # lambda to be called whenever FaradayMiddleware#open_circuit? returns true
      # when the circuit is "open" i.e., the number of times
      # FaradayMiddleware#open_circuit? returns true reaches some threshold,
      # original_response here is nil, hence we return 503 Service Unavailable.
      default_value = lambda do |original_response, original_error|
        original_response || service_unavailable
      end

      auth = options[:auth]

      Faraday.new do |conn|
        if use_circuitbox?
          circuit_breaker_options = { cache: Moneta.new(:Redis, url: ENV["REDIS_CIRCUITBOX_URL"]) }.merge(options.except(:auth))
          conn.use Circuitbox::FaradayMiddleware, circuit_breaker_options: circuit_breaker_options, default_value: default_value
        end
        conn.adapter Faraday.default_adapter
        conn.basic_auth(auth.username, auth.password) if auth
      end
    end

    def use_circuitbox?
      use_circuitbox = Figaro.env.USE_CIRCUITBOX
      use_circuitbox.present? && use_circuitbox.to_i == 1
    end

    private
      def service_unavailable
        response = Response.new
        Faraday::Response.new(status: 503, body: response.to_json)
      end
  end
end
