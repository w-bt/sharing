class ServiceProviderActivation
  def call
    ServiceProvider.where(is_active: false).find_each(batch_size: 100) do |sp|
      sp.update!(is_active: true)
      KafkaPublishing::ServiceProvider.new(sp).publish_async
    end
  end
end
