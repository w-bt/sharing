module Serializers
  module ServiceProvider
    class ForAdmin
      def initialize(service_provider)
        @service_provider = service_provider
      end

      def serialize
        native_attributes.compact
      end

      private

      # rubocop:disable Metrics/MethodLength
      def native_attributes
        native_keys = %i[
          guid full_name email phone_number gender description is_active
        ]
        @service_provider.attributes.symbolize_keys.slice(*native_keys)
      end
      # rubocop:enable Metrics/MethodLength
    end
  end
end
