module Admin
  module ServiceProviders
    class ShowService
      extend Memoist

      def initialize(service_provider_guid)
        @service_provider_guid = service_provider_guid
      end

      def call
        Rails.cache.fetch("service_provider:#{@service_provider_guid}") do
          service_provider = ServiceProvider.find_by_guid(@service_provider_guid)
          raise NotFound unless service_provider
          serializer_class = Serializers::ServiceProvider::ForAdmin
          serializer_class.new(service_provider).serialize
        end
      end
      memoize :call
    end
  end
end
