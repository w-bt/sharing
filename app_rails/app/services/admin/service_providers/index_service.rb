module Admin
  module ServiceProviders
    class IndexService
      extend Memoist

      def initialize(limit, skip)
        @limit = limit
        @skip = skip
      end

      def call
        service_providers = ServiceProvider.limit(@limit)
                              .offset(@skip)
                              .all.order(created_at: :desc)
        serializer_class = Serializers::ServiceProvider::ForAdmin
        service_providers.map do |sp|
          serializer_class.new(sp).serialize
        end
      end
      memoize :call
    end
  end
end
