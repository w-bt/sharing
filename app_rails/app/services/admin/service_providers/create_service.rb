module Admin
  module ServiceProviders
    class CreateService

      def initialize(create_params)
        @create_params = create_params
      end

      def call
        sp = ServiceProvider.create!(@create_params)
        KafkaPublishing::ServiceProvider.new(sp).publish_async
      end
    end
  end
end
