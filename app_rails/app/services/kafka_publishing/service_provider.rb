module KafkaPublishing
  class ServiceProvider
    def initialize(service_provider)
      @service_provider = service_provider
    end

    def publish_async
      message = @service_provider.to_json
      base_encoded_message = Base64.encode64(message)
      base_encoded_key = Base64.encode64("service_provider:#{@service_provider.guid}")
      topic = ENV['KAFKA_SP_TOPIC'].to_s
      KafkaPublishingWorker.perform_async(
        base_encoded_message, base_encoded_key, topic: topic
      )
    end
  end
end
