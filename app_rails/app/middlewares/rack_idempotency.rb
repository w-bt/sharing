class RackIdempotency < Rack::Idempotency
  include RenderError

  def call(env)
    return @app.call(env) if Request.new(env.dup.freeze).get?
    super
  rescue Rack::Idempotency::InsecureKeyError => error
    Rails.logger.error("#{error.class} #{error.message}")
    render_error(:bad_request, error.message)
  end
end
