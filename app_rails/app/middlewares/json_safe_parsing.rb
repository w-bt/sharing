class JsonSafeParsing
  include RenderError

  def initialize(app)
    @app = app
  end

  def call(env)
    @app.call(env)
  rescue ActionDispatch::ParamsParser::ParseError => error
    Rails.logger.error("#{error.class} #{error.message}")
    render_error(:bad_request, error.message)
  end
end
