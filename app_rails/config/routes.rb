require 'sidekiq/web'

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/ping', to: 'application#ping'
  get '/health', to: 'application#health'

  mount Sidekiq::Web => '/sidekiq'

  mount SwaggerUiEngine::Engine, at: "/docs"

  scope module: :api do
    scope module: :v1, path: :v1 do
      namespace :admin do
        resources :service_providers, only: [:index, :show, :create]
      end
    end
  end

  match '*path', to: 'application#route_error', via: :all, as: :route_error
end
