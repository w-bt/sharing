require 'clockwork'
require './config/boot'
require './config/environment'
require 'active_support/time'

module Clockwork
  handler do |job, _time|
    date_string = Time.now.utc.to_date.strftime('%Y-%m-%d')
    if job.eql?('sp_activation.job')
      ServiceProviderActivationWorker.perform_async
    end
    puts "[CLOCKWORK] Spawned #{job} for #{date_string} to worker."
  end

  every(10.seconds, 'sp_activation.job')
end
