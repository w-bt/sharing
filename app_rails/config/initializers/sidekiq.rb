require 'sidekiq-limit_fetch'

Sidekiq.configure_server do |config|
  config.redis = { url: ENV['REDIS_SIDEKIQ_URL'], namespace: ENV["REDIS_NAMESPACE"] }
  config.error_handlers << Proc.new { |ex, ctx_hash| DeliveryBoy.shutdown }
end

Sidekiq.configure_client do |config|
  config.redis = { url: ENV['REDIS_SIDEKIQ_URL'], namespace: ENV["REDIS_NAMESPACE"] }
end

Sidekiq.default_worker_options = { backtrace: true }
